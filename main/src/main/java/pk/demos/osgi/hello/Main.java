package pk.demos.osgi.hello;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.util.*;

import org.osgi.framework.*;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

public class Main {

    private static String bundlesDir = "../bundles";

    public static void main(String[] args) throws Exception {
        try {
            Framework framework = createFramework();
            framework.start();

            List<Bundle> bundles = installBundles(framework.getBundleContext());
            startBundles(bundles);
            startMainClass(bundles);

            framework.stop();
            framework.waitForStop(0);
            System.exit(0);
        } catch (Exception ex) {
            System.err.println("Error starting program: " + ex);
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private static void installShutdownHookForJVM(final Framework framework) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    if (framework != null) {
                        framework.stop();
                        framework.waitForStop(0);
                    }
                } catch (Exception ex) {
                    System.err.println("Error stopping framework: " + ex);
                }
            }
        });
    }

    private static Framework createFramework() {
        ServiceLoader<FrameworkFactory> factoryLoader = ServiceLoader.load(FrameworkFactory.class);
        FrameworkFactory frameworkFactory = factoryLoader.iterator().next();
        Map<String, String> configMap = new HashMap<>();
        configMap.put(Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit");
        Framework framework = frameworkFactory.newFramework(configMap);
        installShutdownHookForJVM(framework);
        return framework;
    }

    private static File[] findAllJars() {
        return new File(bundlesDir).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith("jar");
            }
        });
    }

    private static List<Bundle> installBundles(BundleContext context) throws BundleException {
        List<Bundle> bundles = new ArrayList<>();
        File[] jars = findAllJars();
        for (int i = 0; i < jars.length; i++) {
            Bundle bundle = context.installBundle(jars[i].toURI().toString());
            bundles.add(bundle);
        }
        return bundles;
    }

    private static boolean isFragment(Bundle bundle) {
        return bundle.getHeaders().get(Constants.FRAGMENT_HOST) != null;
    }

    private static void startBundles(List<Bundle> bundles) throws BundleException {
        for (Bundle bundle : bundles) {
            if (!isFragment(bundle))
                bundle.start();
        }
    }

    private static void startMainClass(List<Bundle> bundles) throws Exception {
        for (Bundle bundle : bundles) {
            String mainClassName = bundle.getHeaders().get("Main-Class");
            if (mainClassName != null) {
                Class mainClass = bundle.loadClass(mainClassName);
                Method method = mainClass.getMethod("main", new Class[] { String[].class });
                String[] mainArgs = new String[0];
                method.invoke(null, new Object[] { mainArgs });
                break;
            }
        }
    }
}
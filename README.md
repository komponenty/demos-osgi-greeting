Overview
========

Simple application demonstrating OSGi basics.
Each commit is next step during transformation regular Java application into OSGi driven one.
There are some tags as well which helps jumping to right version.

Tags
----

* base - initial version of application
* modularity - first approach to OSGi application showing modularity layer (using bundles)
* lifecycle - second approach to OSGi application showing lifecycle layer (using activators)
* service - third approach to OSGi application showing service layer (using interfaces and service registry)


How to use
==========

* `gradle run` - runs Java application with embeded OSGi container Felix
* `gradle console` - executes PAX Runner with Felix as OSGi framework and GoGo shell
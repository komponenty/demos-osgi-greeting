package pk.demos.osgi.hello.client;


import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import pk.demos.osgi.hello.provider.Greeting;


public class Client implements BundleActivator {


    private static BundleContext context;

    @Override
    public void start(BundleContext context) throws Exception {
        this.context = context;
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    }

    public static void main(String[] args) {
        ServiceReference ref = context.getServiceReference(Greeting.class.getName());
        ((Greeting)context.getService(ref)).sayHello();
    }
}

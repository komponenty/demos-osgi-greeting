package pk.demos.osgi.hello.provider.impl;


import pk.demos.osgi.hello.provider.Greeting;

public class GreetingImpl implements Greeting {

    private final String name;

    public GreetingImpl(String name) {
        this.name = name;
    }

    @Override
    public void sayHello() {
        System.out.println("Witaj " + name + "!");
    }

}
